<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/gomez', function () {
    return "Hola mundo por GET";
});

Route::post('/gomez', function () {
    return "Hola mundo por POST";
});

Route::get('/','Curso\EjemploController@index');
Route::get('/persona','Curso\EjemploController@persona');
Route::get('/datos/personales/{nombre}/{id?}','Curso\EjemploController@datosPersonales')
->where(['nombre'=>'[a-z A-Z]+','id'=>'[0-9]+'])->name('datos');
Route::get('/datos/personales/{id}','Curso\EjemploController@datosPersonales')->where(['id'=>'[0-9]+']);

Route::get('/datos/personales/','Curso\EjemploController@frmDatosPersonales');
Route::post('/datos/personales','Curso\EjemploController@requestDatosPersonales');
Route::put('/datos/personales','Curso\EjemploController@requestEditDatosPersonales');
//->middleware('check.edad');
Route::get('/persona/eliminar/{id}','Curso\EjemploController@delete');

Route::get('/datos/tablaPersonas/','Curso\EjemploController@tblDatosPersonales')->name('tblPersona');


// Servicios

Route::get('/datos/tablaServicios/','Curso\ServicioController@tblDatosServicios')->name('tblServicios');
Route::get('/datos/servicios/','Curso\ServicioController@frmDatosServicios');
Route::post('/datos/servicios','Curso\ServicioController@requestDatosServicios');
Route::put('/datos/servicios','Curso\ServicioController@requestEditDatosPersonales');

Route::get('/datos/servicios/{id}','Curso\ServicioController@datosServicios')->where(['id'=>'[0-9]+']);
Route::put('/datos/servicios','Curso\ServicioController@requestEditDatosServicios');
Route::get('/servicio/eliminar/{id}','Curso\ServicioController@delete');
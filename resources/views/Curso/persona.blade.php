@extends('Layout.padre')

@section('title')
    {{$title}}
@endsection

@section('content')

    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-10">

        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Gustos Musicales
                    </button>
                </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <ul>
                       <li>Rock 
                            <ul>
                                <li>Black sabbath</li>
                                <li>Metallica</li>
                                <li>AC/DC</li>
                                <li>Rolling Stones</li>
                            </ul>
                       </li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Curriculum Academico
                    </button>
                </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                            <ul>
                                <li>Ingeniería informática</li>
                                <li>Maestría en Tecnologías de la Inforamción</li>
                            </ul>
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Conocmientos de Programción
                    </button>
                </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                            <ul>
                                @foreach ($conocmientos_programcion as $conocimiento)
                                    <li>{{ $conocimiento }}</li>
                                @endforeach
                            </ul>
                </div>
                </div>
            </div>
            </div>
        
        
        
        </div>
    </div>

@endsection
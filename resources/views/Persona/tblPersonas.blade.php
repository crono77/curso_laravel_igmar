@extends('Layout.padre')

@section('title')
Tabla
@endsection

@section('content')
<div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">

        <table class="table">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Edad</th>
              <th scope="col">Comentario</th>
              <th scope="col">Opciones Adicionales</th>
            </tr>
          </thead>
          <tbody>

            @foreach($personas as $persona)
                <tr>
                    <td>{{$persona->nombre}}</td>
                    <td>{{$persona->edad}}</td>
                    <td>{{$persona->comentario}}</td>
                    <td>
                    <a href="/datos/personales/{{$persona->id}}">
                      <i data-feather="edit"></i>
                    </a>
                    </td>
                    <td>
                    <a href="/persona/eliminar/{{$persona->id}}">
                      <i data-feather="trash"></i>
                    </a>
                    </td>
                </tr>
            @endforeach

          
          </tbody>
        </table>

</div>

</div>



@endsection
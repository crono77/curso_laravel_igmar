@extends('Layout.padre')

@section('title')
Editar
@endsection

@section('content')

   <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">

    <div class="card">
        <div class="card-header">
            Formulario Datos Personales
        </div>
        <div class="card-body">
        <form method="POST" action="/datos/personales">
        @method('PUT')

        {{--@method('PUT')--}}
        @csrf
            <input type="hidden" value="{{$persona->id}}" name="id">
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" value="{{$persona->nombre}}" placeholder="Teclea tu Nombre..">
            </div>
            <div class="form-group">
                <label for="edad">Edad</label>
                <input type="text" class="form-control" id="edad" name="edad" value="{{$persona->edad}}" placeholder="Teclea tu Edad..">
            </div>
            <div class="form-group">
                <label for="comentario">Comentario</label>
                <textarea class="form-control" id="comentario" name="comentario" rows="3" placeholder="Escribe un comentario">
                    {{$persona->comentario}}
                </textarea>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>

        </div>
    </div>

    </div>
   </div>

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@endsection
@extends('Layout.padre')

@section('title')
Tabla
@endsection

@section('content')
<div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">

        <table class="table">
          <thead>
            <tr>
              <th scope="col">Nombre del Servicio</th>
              <th scope="col">Costo</th>
              <th scope="col">Comentario</th>
              <th scope="col">Opciones Adicionales</th>
            </tr>
          </thead>
          <tbody>

            @foreach($servicios as $servicio)
                <tr>
                    <td>{{$servicio->nombre_servicio}}</td>
                    <td>{{$servicio->costo}}</td>
                    <td>{{$servicio->comentario}}</td>
                    <td>
                    <a href="/datos/servicios/{{$servicio->id}}">
                      <i data-feather="edit"></i>
                    </a>
                    </td>
                    <td>
                    <a href="/servicio/eliminar/{{$servicio->id}}">
                      <i data-feather="trash"></i>
                    </a>
                    </td>
                </tr>
            @endforeach

          
          </tbody>
        </table>

</div>

</div>



@endsection
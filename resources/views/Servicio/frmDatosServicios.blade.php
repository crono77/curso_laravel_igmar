@extends('Layout.padre')

@section('title')
Editar
@endsection

@section('content')

   <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">

    <div class="card">
        <div class="card-header">
            Formulario Datos Servicio
        </div>
        <div class="card-body">
        <form method="POST" action="/datos/servicios">

        @csrf
            <div class="form-group">
                <label for="nombre_servicio">Nombre Servicio</label>
                <input type="text" class="form-control" id="nombre_servicio" name="nombre_servicio"  placeholder="Teclea el Nombre del Servicio..">
            </div>
            <div class="form-group">
                <label for="costo">Costo</label>
                <input type="text" class="form-control" id="costo" name="costo" placeholder="Teclea el Costo del Servicio..">
            </div>
            <div class="form-group">
                <label for="comentario">Comentario</label>
                <textarea class="form-control" id="comentario" name="comentario" rows="3" placeholder="Escribe un Comentario.."></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>

        </div>
    </div>

    </div>
   </div>

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@endsection
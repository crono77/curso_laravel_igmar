@extends('Layout.padre')

@section('title')
Editar
@endsection

@section('content')

   <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">

    <div class="card">
        <div class="card-header">
            Formulario Datos Servicio
        </div>
        <div class="card-body">
        <form method="POST" action="/datos/servicios">
        @method('PUT')

        @csrf
            <input type="hidden" value="{{$servicio->id}}" name="id">
            <div class="form-group">
                <label for="nombre_servicio">Nombre Servicio</label>
                <input type="text" class="form-control" id="nombre_servicio" name="nombre_servicio" value="{{$servicio->nombre_servicio}}" placeholder="Teclea tu Nombre..">
            </div>
            <div class="form-group">
                <label for="costo">Costo</label>
                <input type="text" class="form-control" id="costo" name="costo" value="{{$servicio->costo}}" placeholder="Teclea el costo del Serivio">
            </div>
            <div class="form-group">
                <label for="comentario">Comentario</label>
                <textarea class="form-control" id="comentario" name="comentario" rows="3" placeholder="Escribe un comentario">{{$servicio->comentario}}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>

        </div>
    </div>

    </div>
   </div>

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@endsection
<?php

namespace App\Http\Controllers\Curso;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Servicio;

class ServicioController extends Controller
{
    public function tblDatosServicios(){
        $servicios = Servicio::all();

        return view('Servicio.tblServicios',["servicios"=>$servicios]);

    }

    public function frmDatosServicios(Request $request){

        return view("Servicio.frmDatosServicios");
        
    }

    public function requestDatosServicios(Request $request){

     
        $request->validate([
            'nombre_servicio'=>'required',
            'costo'=>'required|numeric',
        ],[
            'nombre_servicio.required'=>'El campo Nombre es Obligatorio',
            'costo.required'=>'El campo Costo es Obligatotio',
            'costo.numeric'=>'El campo Costo debe ser Numerico'
        ]);

        $servicio = new Servicio();
        $servicio->nombre_servicio = $request->nombre_servicio;
        $servicio->costo = $request->costo;
        $servicio->comentario = $request->comentario;

        $servicio->save();

        return redirect()->route('tblServicios');
    }


    public function datosServicios($id){
        $servcio = Servicio::find($id);
        return view('Servicio.frmDatosEditServicios', ["servicio"=>$servcio]);
    }


    public function requestEditDatosServicios(Request $request){

     
        $request->validate([
            'id'=>'required',
            'nombre_servicio'=>'required',
            'costo'=>'required|numeric',
        ],[
            'nombre_servicio.required'=>'El campo Nombre es Obligatorio',
            'costo.required'=>'El campo Costo es Obligatotio',
            'costo.numeric'=>'El campo Costo debe ser Numerico'
        ]);

        $servicio = Servicio::find($request->id);
        $servicio->nombre_servicio = $request->nombre_servicio;
        $servicio->costo = $request->costo;
        $servicio->comentario = $request->comentario;

        $servicio->save();

        return redirect()->route('tblServicios');
    }


    public function delete(Request $request){
        $servicio = Servicio::find($request->id);
        $servicio->delete();
        return redirect()->route('tblServicios');
    }

}

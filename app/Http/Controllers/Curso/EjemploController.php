<?php

namespace App\Http\Controllers\Curso;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Persona;

class EjemploController extends Controller
{
    public function index(Request $request){
        $msg = "Beinvenido a mi primer controlador !!";
        return view("Curso.index",[
            "ip"=>$request->ip(),
            "mensaje"=>$msg,
            "title"=>"Beinvenido !!",
        ]);
    }

    public function persona(Request $request){
        return view("Curso.persona",[
            "title"=>"Persona",
            'conocmientos_programcion'=>['C#','C++','PHP','Laravel','Python','Django','JavaScript','NodeJs']
        ]);
    }

    public function datosPersonales($id){
        $persona = Persona::find($id);

        return view('Persona.frmEdirDatosPersonales', ["persona"=>$persona]);
        
    }


    public function frmDatosPersonales(Request $request, $nombre, $id=null){

        return view("Curso.frmDatosPersonales");
        
    }


    public function requestEditDatosPersonales(Request $request){
            $request->validate([
                'id'=>'required',
                'nombre'=>'required',
                'edad'=>'required|numeric',
            ],[
                'nombre.required'=>'El campo Nombre es Obligatorio',
                'edad.required'=>'El campo Edad es Obligatotio',
                'nombre.numeric'=>'El campo Edad debe ser numerico'
            ]);

            $persona = Persona::find($request->id);

            $persona->nombre = $request->nombre;
            $persona->edad = $request->edad;
            $persona->comentario = $request->comentario;

            $persona->save();

            return redirect()->route('tblPersona');
    }

    public function delete(Request $request){

        $persona = Persona::find($request->id);

        $persona->delete();

        return redirect()->route('tblPersona');
    }

    public function requestDatosPersonales(Request $request){

     
        $request->validate([
            'nombre'=>'required',
            'edad'=>'required|numeric',
        ],[
            'nombre.required'=>'El campo Nombre es Obligatorio',
            'edad.required'=>'El campo Edad es Obligatotio',
            'nombre.numeric'=>'El campo Edad debe ser numerico'
        ]);

        $persona = new Persona();
        $persona->nombre = $request->nombre;
        $persona->edad = $request->edad;
        $persona->comentario = $request->comentario;

        $persona->save();

        return redirect()->route('tblPersona');

        //return back();

        ///return redirect()->route('datos',["nombre"=>$request->nombre,"edad"=>$request->edad]);
    }


    public function tblDatosPersonales(){
            $personas = Persona::all();

            return view('Persona.tblPersonas',["personas"=>$personas]);

    }
}
